#!/bin/bash
#set -o errexit # Bail out on all errors immediately

echo "---${THIS_CONTAINER}---"

case $DEPLOYMENT_TYPE in
  "kubernetes")
    # Print PodInfo
    echo ""
    echo "%%%--- PodInfo ---%%%"
    echo "Pod namespace: ${PODINFO_NAMESPACE}"
    echo "Pod name: ${PODINFO_NAME}"
    echo "Pod IP: ${PODINFO_IP}"
    echo "Node name (of the host where the pod is running): ${PODINFO_NODE_NAME}" 
    echo "Node IP (of the host where the pod is running): ${PODINFO_NODE_IP}"

    echo "Deploying with configuration for Kubernetes..."
    sh /root/pullimages.sh $DOCKER_IMAGES
    if [ "$DAEMON_SET" == "true" ]; then
      echo ""
      echo "Running as DaemonSet. Entering idle mode..."
      sleep infinity
    fi
    ;;

  "compose")
    echo "Deploying with configuration for Docker Compose..."
    sh /root/pullimages.sh $DOCKER_IMAGES
    ;;

  *)
    echo "ERROR: Deployment context is not defined."
    echo "Cannot continue."
    exit -1
esac

