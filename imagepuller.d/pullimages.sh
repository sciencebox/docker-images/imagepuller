#!/bin/bash

if [ "$#" -lt 1 ]; then
        echo "ERROR: Illegal number of parameters."
        echo "Syntax: imagepuller.sh <docker_image_1> <docker_image_2> ... <docker_image_N>"
        echo "Example: imagepuller.sh docker:stable"
        exit -1
fi

RETRY_LIMIT=12
DOCKER_IMAGES="$@"
echo "Pulling images: $DOCKER_IMAGES"


pull_the_image ()
{
  IMAGE=$1
  echo "Pulling: $IMAGE"
  if [ "$(docker images -q $IMAGE)" == "" ]; then
    docker pull $IMAGE
  else
    echo "Image $IMAGE already exists."
  fi
}


FAILED=true
RETRY_COUNT=0
while $FAILED && [ $RETRY_COUNT -le $RETRY_LIMIT ];
do
  FAILED=false
  for i in $DOCKER_IMAGES
  do
    pull_the_image $i
    if [ `echo $?` -ne 0 ]; then
      FAILED=true
      echo "Error while pulling $i"
    fi
  done

  if $FAILED; then
    SLEEP_TIME=$(echo "2^$RETRY_COUNT" | bc -l)
    echo ""
    echo "Errors occurred while pulling images"
    echo "Retrying in $SLEEP_TIME secs..."
    sleep $SLEEP_TIME
  fi
  RETRY_COUNT=$((RETRY_COUNT+1))
done

if $FAILED; then
  echo "Errors occurred while pulling images."
else
  echo "All images were successfully pulled."
fi
