#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-#
#       |S|c|i|e|n|c|e| |B|o|x|        #
#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-#

# Docker file for imagepuller

# Note: only tag 'latest' is supported

# Build and push to Docker registry with:
#   docker build -t gitlab-registry.cern.ch/sciencebox/docker-images/imagepuller -f imagepuller.Dockerfile .
#   docker login gitlab-registry.cern.ch
#   docker push gitlab-registry.cern.ch/sciencebox/docker-images/imagepuller


FROM docker:stable

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Copy the scripts to pull the required Docker images ----- #
ADD ./imagepuller.d/start.sh /root/
ADD ./imagepuller.d/pullimages.sh /root/

# ----- Run the script in the container ----- #
ENTRYPOINT []
CMD ["sh", "/root/start.sh"]

